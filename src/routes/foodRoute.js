const express = require('express');
const foodRoute = express.Router();

const { getFoodDemo } = require('../controllers/foodController');

foodRoute.get("/demo", getFoodDemo);

module.exports = foodRoute;